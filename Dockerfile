FROM nginx:latest

#Remove default.conf
RUN rm -rf /etc/nginx/conf.d/default.conf

#Copy our configs to nginx/conf.d
COPY /conf.d/* /etc/nginx/conf.d/
